# idevfsdsrv1

<!-- TOC titleSize:2 tabSpaces:3 depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 skip:1 title:0 -->

- [À propos](#-propos)
- [Système](#systme)
- [Pré-Requis](#pr-requis)
   - [Docker](#docker)
   - [Docker-compose](#docker-compose)
- [Emplacement des fichiers](#emplacement-des-fichiers)
- [Première mise en place](#premire-mise-en-place)
   - [Définition des variables propre au projet](#dfinition-des-variables-propre-au-projet)
   - [Construction des containers](#construction-des-containers)
   - [Base de donnée](#base-de-donne)
      - [Importation de données de test (seed)](#importation-de-donnes-de-test-seed)

<!-- /TOC -->

# À propos
Ceci liste les étapes nécessaires pour la mise en place du projet sur
idevfsdsrv1, un machine virtuelle installée avec Ubuntu 18.04.2 LTS.

On considère que le serveur est déjà pré-installé avec de manière minimale, et
que l'utilisateur a un accès ssh ainsi que les droits "root", e.g. `ssh
root@idevfsdsrv1`.

# Système
```
root@idevfsdsrv1:/# lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.2 LTS
Release:	18.04
Codename:	bionic
```

* IP: 128.178.50.39
* Hostname: idevfsdsrv1
* Domain: epfl.ch
* Alias (CNAME): go-new

# Pré-Requis
Certain logiciels sont nécessaires au bon fonctionnement du système et au
confort des utilisateurs. La commande ci-dessous permet de les installer:
`apt install -y git make tmux net-tools`

## Docker
https://docs.docker.com/install/linux/docker-ce/ubuntu/
```
apt remove docker docker-engine docker.io containerd runc
apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt update
apt install docker-ce docker-ce-cli containerd.io
````

## Docker-compose
https://github.com/docker/compose/releases/
```
curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
 
# Emplacement des fichiers
Afin de favoriser le maintien de l'application par plusieurs personnes, créer le
repertoire `/srv/go.epfl.ch`. Il contiendra les sources de l'application.

Cloner le repository dans ce dossier:
`git clone git@gitlab.com:epfl-idevfsd/go-epfl.git /srv/go.epfl.ch`

# Première mise en place
Afin de construire/télécharger une première version des différents containers de
l'application:
```
cd /srv/go.epfl.ch
make build
```

Cette étape prend quelques minutes, vous pouvez commencer la prochaine en
attendant.

## Définition des variables propre au projet
Dans le fichier `.env` modifier la valeur du fichier docker-compose, par
exemple `docker-compose-dev.yml` ou `docker-compose-prod.yml`. Il faut également
s'assurer des valeurs pour la connexion à la base postgres.


## Construction des containers
Ceci est l'équivalent d'un docker-pull ou docker-build selon les Dockerfiles:
`make build`

Afin que node, composer ou artisan puissent écrire les fichiers des dépendances,
s'assurer que les permissions soient celles de l'utilisateur "laradock":
`make perms`

Après, installer les dépendances de composer avec la commande:
`make composer CMD=update`

Ensuite, faire de même pour installer les dépendances de node (`./node_modules`):
`make workspace CMD='npm i'`

Finalement, générer la clé de l'application avec la commande artisan suivante:
`make artisan CMD='key:generate'`

## Base de donnée
Avant toute chose, il convient de créer la base de donnée selon variables
présentes dans le fichier point env:
`make artisan CMD='db:create'`

Laravel permet de gérer les versions du schéma de base de donnée avec un
mécanisme de migration. Dans le but d'appliquer les différents changements et
d'avoir un schéma à jour, appliquer la commande:
`make artisan CMD='migrate'`

### Importation de données de test (seed)
[WIP to discuss with @loichu]
