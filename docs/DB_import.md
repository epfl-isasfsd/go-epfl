# DB Import

This file list the steps required to import old go.epfl.ch MySQL data into the
new PostgeSQL DB.

## data extracter
1. [ ] ...
1. [ ] ...
1. [ ] ...
1. [ ] move the SQL dump in `data/postgres` repository:  
   `sudo cp dump.sql [...]/gonew/data/postgres`

## data import
1. [ ] clear the old data:  
   `just artisan migrate:fresh`
1. [ ] connect to the postgres container:  
   `just postgres bash`
1. [ ] import the data:  
   `psql goepfl < dump.sql`
