# goepflbot

I am the go.epfl.ch bot - I ❤️ tweets that contain links to go.epfl.ch.

You can see my likes on https://twitter.com/goepflbot/likes.

## Usage

1. Create the `.env` file based on `.env.sample`.  
   Secrets are obtained from [https://developer.twitter.com/en](https://developer.twitter.com/en).
2. `docker-compose -f docker-compose-dev.yml up` or `npm start`.
