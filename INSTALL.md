# Install

## Task runner (Just)

The task runner used to simplify maintenance is [just]. It is written in `Rust`
so you can either [install it via Cargo](https://github.com/casey/just#cargo) or
just [download the binaries](https://github.com/casey/just#pre-built-binaries).

The recommended way is to use `Cargo` as it is the official compiler and package
manager for `Rust` and it is really easy to install. Everything is very
straight-forward.

To connect to Tequila authentication, you'll need a `CLIENT_ID` and
`CLIENT_SECRET`. If you have access to the team's keybase repository, you can
find them here, otherwise you won't be able to authenticate unless you change
the code for another authentication system.


## Develop GO

1. `git clone git@gitlab.com:epfl-isasfsd/go-epfl.git go-dev`
1. `cd go-dev`
1. `cp .env.example .env`
1. `cp go-app/.env.example go-app/.env`
1. In both `.env` and `go-app/.env` edit the variables, especially
   - `TEQUILA_*`
   - `APP_KEY`
   - `MAIL_*`
   - `TG_*`
   - `SECRETS*`
1. Plese note that you can found these in `/keybase/team/goepfl/`
1. According that you've installed [just], run  
   `just install`
1. At this step you should have fews dockers running
1. The Justfile will attempt to create a `go-dev.epfl.ch` entry in your 
   `/etc/hosts` meaning that you can head to https://go.epfl.ch to see if it's
   working now 👊

Note: If you need a cleaned Docker environment, or want to restart from scratch, use
the command `$ just reinstall`.

### Dealing with local certificates

[Traefik] is using the certificate named `go-dev.epfl.ch{*}.pem` in 
`docker/traefik/certs`. While it will offer a self-signed certificate, you will 
have to accept it a lot during developping go. A nice and simple solution is to
use [mkcert]. You can install it with the 
[pre-built binaries](https://github.com/FiloSottile/mkcert/releases). Once done,
run:
1. `cd go-dev/docker/traefik/certs`
1. `just stop` to pause the application
1. Remove `go-dev.epfl.ch{*}.pem` folders if present
1. Run `mkcert go-dev.epfl.ch` to create the certificate
1. Run `mkcert -install` to install this certificate in your system (Chrome, Firefox, ...)
1. `just up`




[just]: https://github.com/casey/just
[Traefik]: https://traefik.io
[mkcert]: https://github.com/FiloSottile/mkcert
