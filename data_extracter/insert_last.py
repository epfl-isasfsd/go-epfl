from postgres_conn import PostgresDB
from mysql_conn import MySqlDB
import time
from extract import Extract
import insert

COMMIT_BATCH_DELAY_SECONDS = 10

extract = Extract()

exec(open("db-conf.py").read())
old_db = MySqlDB(config_old_db)
new_db = PostgresDB(config_new_db)

#
# ALIASES
#
if True:
    print('Preparing aliases...')
    start = time.time()
    last_inserted_date = extract.last_alias_date(new_db)
    print('Looking for aliases inserted after ' + str(last_inserted_date[0]))
    aliases = extract.last_urls(old_db, str(last_inserted_date[0]))
    for alias in aliases:
        alias['url_id'] = extract.url_id(new_db, alias['url'])
        if alias['url_id'] == None:
            alias['url_id'] = insert.url(new_db, alias)
    end = time.time()
    elapsed = end - start
    print('Aliases ready in ' + str(elapsed) + ' seconds')

    print('Inserting aliases...')
    start = time.time()
    for alias in aliases:
        insert.alias(new_db, alias)
    end = time.time()
    elapsed = end - start
    print('Aliases inserted in ' + str(elapsed) + ' seconds')

#
# LOGS
#
def prepare_logs():
    print('Preparing logs...')
    start = time.time()
    last_inserted_date = extract.last_log_date(new_db)
    removed_aliases = []
    logs_ready = []
    for log in extract.last_logs(old_db, last_inserted_date):
        shorturl = log['alias']
        if shorturl in removed_aliases: continue
        alias = extract.alias_details(new_db, shorturl) or extract.code_details(new_db, shorturl)
        if alias is None:
            removed_aliases.append(shorturl)
            print(shorturl + ' has been removed, no logs for this one')
            continue
        log['alias_id'] = alias[0]
        log['url'] = alias[1]
        yield log
    end = time.time()
    elapsed = end - start
    print('Logs ready in ' + str(elapsed) + ' seconds')

print('Inserting logs...')
start = time.time()
req_count = 0
last_commit_time = time.time()
for log in prepare_logs():
    insert.log(new_db, log)
    req_count += 1
    elapsed_txn = time.time() - last_commit_time
    if elapsed_txn >= COMMIT_BATCH_DELAY_SECONDS:
        print("Committing %d rows (%f elapsed)..." % (req_count, elapsed_txn))
        new_db.commit()
        req_count = 0
        last_commit_time = time.time()
new_db.commit()
end = time.time()
elapsed = end - start
print('Logs inserted in ' + str(elapsed) + ' seconds')
