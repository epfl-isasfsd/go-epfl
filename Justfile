# This file contains the commands that are usually run for deployment and maintenance.
# https://github.com/casey/just

# The following lines enable syntax highlighting for vim and emacs:
# Local Variables:
# mode: makefile
# End:
# vim: set ft=make :

# Brings up application
install: check-env
        just build
        just fix-perms
        just up
        just composer update
        just npm install
        if [ ! -f $APP_CODE_DIRECTORY/.env ]; then \
            just workspace cp .env.example .env; fi
        just artisan key:generate
        just artisan db:create
        just artisan migrate
        just artisan db:seed
        just artisan go-stats:refresh
        just npm run production
        just proxy_on
        # https://stackoverflow.com/a/52330607/960623
        if [ ! -d $APP_CODE_DIRECTORY/storage/framework/cache/data ]; then \
           mkdir -p $APP_CODE_DIRECTORY/storage/framework/cache/data; fi
        just dumbfix

# Gets rid of application
uninstall: down clear-data clear-all
        sudo sed -i '/go.epfl.ch/d' /etc/hosts

# Uninstalls then installs again
reinstall: uninstall install

##
## Filesystem operations
##

# Checks that .env file exists
@check-env:
        [ ! -f .env ] && \
        echo "Please create your .env file first, from .env.example" \
        || exit 0

# Checks that reverse proxy is available on host
@check-rproxy: check-env
        if [ $APP_ENV = 'production' ]; then exit 0; fi
        grep "$APP_NAME" -q /etc/hosts && \
        exit 0 \
        || just setup-host

# Sets the user with id $WORKSPACE_PUID as owner for a given directory
@chown directory:
        [ -e {{directory}} ] && \
        sudo chown -R $WORKSPACE_PUID:$WORKSPACE_PGID {{directory}} \
        || exit 0

alias on := proxy_on
# Activates the reverse proxy on host so that go.epfl.ch points to localhost:8000
@proxy_on: check-rproxy
        set -e -x
        if [ $APP_ENV = 'production' ]; then exit 0; fi
        sudo sed -i '/'"$APP_NAME"'/s/^#//g' /etc/hosts;\

alias off := proxy_off
# Dectivates the reverse proxy on host so that go.epfl.ch points to internet
@proxy_off: check-rproxy
        set -e -x
        if [ $APP_ENV = 'production' ]; then exit 0; fi
        sudo sed -i '/'"$APP_NAME"'/s/^/#/g' /etc/hosts;\

# Prepares host DNS for development on localhost
setup-host:
        if [ $APP_ENV = 'production' ]; then exit 0; fi
        echo "127.0.0.1 $APP_NAME" | sudo tee -a /etc/hosts > /dev/null


# Fixes "Permission denied", only for development
@fix-perms:
        just chown $APP_CODE_DIRECTORY
        #just chown $DOCKER_DIRECTORY/nginx/ssl
        #mkdir -p data/prometheus || true
        #just chown data/prometheus

# Clears data directory
clear-data:
        sudo rm -rf data/*

##
## Docker-Compose operations
##

# Runs any docker-compose command
compose +COMMAND: check-env
        docker-compose -f $COMPOSE_FILE {{COMMAND}}

# Runs docker-compose build
build +OPTIONS='': check-env
        just compose build {{OPTIONS}}

# Runs docker-compose down
down:
        just compose down

# Runs docker-compose up in detached mode
up +OPTIONS='':
        just compose up -d {{OPTIONS}}

# Runs docker-compose ps
ps: check-env
        just compose ps

alias log := logs
# Runs docker-compose logs -f
logs: check-env
        just compose logs -f

# Runs docker-compose exec
exec +COMMAND='bash': check-env
        just compose exec {{COMMAND}}

##
## Docker operations
##

alias ps_all := ps-all
# Shows all containers related to this project
ps-all: check-env
        docker ps -a --filter "name=$COMPOSE_PROJECT_NAME"

# Clears all volumes, containers and images
clear-all: check-env
        docker ps -aq --filter "name=${COMPOSE_PROJECT_NAME}" \
        | xargs -r docker rm -f
        docker volume list -q --filter "name=${COMPOSE_PROJECT_NAME}" \
        | xargs -r docker volume rm -f
        docker images -q "${COMPOSE_PROJECT_NAME}*" \
        | xargs -r docker rmi -f

##
## Application logs
##

# Shows Laravel logs
logs-laravel: check-env fix-perms
        tail -n 200 -f $APP_CODE_DIRECTORY/storage/logs/laravel-$(date -u +"%Y-%m-%d").log

# Shows SQL logs
logs-sql: check-env
        sudo tail -n 200 -f $DATA_DIRECTORY_HOST/postgres/pg_logs/postgresql-$(date -u +"%Y-%m-%d").log

##
## Laravel operations
##

# Runs a command as user $WORKSPACE_USER in workspace. (Default: bash)
workspace +COMMAND='bash':
        just exec --user $WORKSPACE_USER workspace {{COMMAND}}

postgres +COMMAND='psql':
        docker-compose exec --user postgres postgres {{COMMAND}}

# export a pg_dump to /data/postgres/dump
dump:
	if [ -d "/var/lib/postgresql/data/dump" ]; then \
	mkdir /var/lib/postgresql/data/dump; \
	fi
	#docker-compose exec -T --user postgres postgres pg_dump --create goepfl -f /var/lib/postgresql/data/dump/goepfl-$(date -u +"%Y-%m-%d_%H%M").dump
	just postgres pg_dump --create goepfl -f /var/lib/postgresql/data/dump/goepfl-$(date -u +"%Y-%m-%d_%H%M").dump

restore:
        just postgres dropdb -e -i --if-exists goepfl; \
        docker-compose exec --user postgres postgres bash -c 'psql < $(find /var/lib/postgresql/data/dump/ -name "*.dump" -type f | tail -n 1)'; \

# Runs artisan in workspace container
artisan +COMMAND:
        just workspace php artisan {{COMMAND}}

# Runs composer in workspace container
composer +COMMAND:
        just workspace composer {{COMMAND}} --verbose

# Runs dusk tests
dusk:
        export APP_URL=https://go-dev.epfl.ch;cd $APP_CODE_DIRECTORY; php artisan dusk --debug -vvv

# Runs behat tests
behat +ARGUMENTS='':
        just workspace php vendor/bin/behat {{ARGUMENTS}}

behat-feature feature:
        just workspace php vendor/bin/behat \
        -- features/{{feature}}.feature

# Runs PHPUnit tests (Laravel standard)
phpunit:
        just composer test

# Runs all tests
test: phpunit dusk

# Clears cache and updates autoloader
dumbfix:
        just artisan cache:clear
        just artisan clear-compiled
        just artisan config:clear
        just artisan route:clear
        just artisan view:clear
        just composer dump-autoload

# Add Laravel cache to all levels
optimize-perf:
        just artisan config:cache
        just artisan route:cache
        just artisan optimize --force
        just composer dump-autoload -o
        just npm run production

# Force refresh materialized view
refresh-materialized:
        just artisan go-stats:refresh

# Prints php_info() information
php-info:
        just workspace php -i

# Install node package in Laravel application
npmi package:
        just npm install {{package}}

# Runs npm in workspace container
npm +COMMAND:
        just workspace npm {{COMMAND}}
