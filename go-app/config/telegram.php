<?php
// Telegram configuration
return [
  'bot_token' => env('TG_BOT_TOKEN', 'tg://user?id=botFather'),
  'public_recipients' => env('TG_PUBLIC_RECIPIENTS'),
  'admin_recipients' => env('TG_ADMIN_RECIPIENTS', 9917772),
]
?>
