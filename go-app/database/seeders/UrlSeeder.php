<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Url;

class UrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Insert nbo's URL
        Url::firstOrCreate(
            ['url' => 'http://people.epfl.ch/169419'],
            [
                'created_at' => '2010-08-18 08:19:19',
                'updated_at' => '2019-07-25 14:15:16',
                'url' => 'http://people.epfl.ch/169419',
                'status_code' => '302',
                'tested_at' => '2021-02-09 00:12:00',
            ]
        );

        // Insert EPFL magazine URL
        Url::firstOrCreate(
            ['url' => 'https://www.epfl.ch/campus/services/communication/audiences-canaux/epfl-magazine/'],
            [
                'created_at' => '2019-07-31 12:41:14',
                'updated_at' => '2019-07-31 12:41:14',
                'url' => 'https://www.epfl.ch/campus/services/communication/audiences-canaux/epfl-magazine/',
                'status_code' => '301',
                'tested_at' => '2021-02-09 01:13:00',
            ]
        );

        // Insert Zombo URL
        Url::firstOrCreate(
            ['url' => 'https://zombo.com'],
            [
                'created_at' => '2019-09-05 15:43:05',
                'updated_at' => '2019-09-05 15:43:05',
                'url' => 'https://zombo.com',
                'status_code' => '200',
                'tested_at' => '2021-02-10 01:13:00',
            ]
        );

        // Insert menu URL
        Url::firstOrCreate(
            ['url' => 'https://menus.epfl.ch/cgi-bin/getMenus?&midisoir=midi'],
            [
                'created_at' => '2010-01-05 23:33:48',
                'updated_at' => '2019-07-25 14:15:16',
                'url' => 'https://menus.epfl.ch/cgi-bin/getMenus?&midisoir=midi',
                'status_code' => '200',
                'tested_at' => '2021-02-10 00:07:00',
            ]
        );

        // Insert coronavirus URL
        Url::firstOrCreate(
            ['url' => 'https://www.epfl.ch/campus/security-safety/sante/coronavirus-covid-19/'],
            [
                'created_at' => '2020-02-25 14:29:15',
                'updated_at' => '2020-02-25 14:29:15',
                'url' => 'https://www.epfl.ch/campus/security-safety/sante/coronavirus-covid-19/',
                'status_code' => '200',
                'tested_at' => '2020-12-20 02:16:00',
            ]
        );

        // Insert gofi URL
        Url::firstOrCreate(
            ['url' => 'http://flashinformatique.epfl.ch/spip.php?article2176'],
            [
                'created_at' => '2011-06-06 13:59:26',
                'updated_at' => '2019-07-25 14:15:16',
                'url' => 'http://flashinformatique.epfl.ch/spip.php?article2176',
                'status_code' => '302',
                'tested_at' => '2021-02-10 00:11:00',
            ]
        );

        // Never Gonna Give You Up
        Url::firstOrCreate(
            ['url' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'],
            [
                'created_at' => '2009-10-25 13:37:42',
                'updated_at' => '2009-10-25 13:37:42',
                'url' => 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
                'status_code' => '200',
                'tested_at' => '2021-02-10 00:11:00',
            ]
        );

        // Kermit
        Url::firstOrCreate(
            ['url' => 'https://fr.wikipedia.org/wiki/Kermit_la_grenouille'],
            [
                'created_at' => '1955-05-09 13:37:42',
                'updated_at' => '1955-05-09 13:37:42',
                'url' => 'https://fr.wikipedia.org/wiki/Kermit_la_grenouille',
                'status_code' => '200',
                'tested_at' => '2021-02-10 00:11:00',
            ]
        );

        // A very long one
        Url::firstOrCreate(
            ['url' => 'http://a.b.c.d.e.f.g.h.i.j.k.l.m.n.oo.pp.qqq.rrrr.ssssss.tttttttt.uuuuuuuuuuu.vvvvvvvvvvvvvvv.wwwwwwwwwwwwwwwwwwwwww.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy.zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz.me/'],
            [
                'created_at' => '2021-11-03 13:37:42',
                'updated_at' => '2021-11-03 13:37:42',
                'url' => 'http://a.b.c.d.e.f.g.h.i.j.k.l.m.n.oo.pp.qqq.rrrr.ssssss.tttttttt.uuuuuuuuuuu.vvvvvvvvvvvvvvv.wwwwwwwwwwwwwwwwwwwwww.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy.zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz.me/',
                'status_code' => '200',
                'tested_at' => '2021-11-04 00:11:00',
            ]
        );
    }
}
