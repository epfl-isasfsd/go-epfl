<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\BlacklistItem;

class BlackListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
          'bit.do',
          'bit.ly',
          'boobs',
          'casino',
          'cli.co',
          'cutt.ly',
          'dQw4w9WgXcQ',
          'is.gd',
          'llibs.tk',
          'Lrj2Hq7xqQ8',
          'owl.ly',
          'porn',
          'sexe',
          'sttt.tk',
          'tiny.cc',
          'tinyurl',
          'viagra',
          'xill.tk',
        ];

        foreach ($items as $key => $item) {
            BlacklistItem::firstOrCreate(
                ['keyword' => $item],
                [
                  'keyword' => $item,
                  'created_at' => '2021-01-01 13:37:00.000',
                  'updated_at' => '2021-01-01 13:37:00.000',
                ]
            );
        } 
    }
}
