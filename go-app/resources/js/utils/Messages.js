import React from "react";
import { Link } from "react-router-dom";
import LoadingSpinner from "./loading-spinner/LoadingSpinner"

export const AlertSuccess = (props) => (
  <div className="alert alert-success" role="alert" style={{ width: "98%" }}>
    {props.message}
  </div>
);

export const AlertWarning = (props) => (
  <div className="alert alert-warning" role="alert" style={{ width: "98%" }}>
    {props.message}
  </div>
);

export const Loading = () => <LoadingSpinner/>;
