import React, { Fragment, Component } from "react";
import Pagination from "react-js-pagination";
import { getRessource } from "../utils/api";
import { Loading } from "../utils/Messages";
import queryString from "query-string";

class Alias extends Component {
  constructor(props) {
    super();
    let params = queryString.parse(props.location.search);
    this.state = {
      aliases: [],
      total: 0,
      currentPage: 1,
      perPage: 10,
      keyword: "",
      searchOwner: params.owner,
      sortedField: "",
      sortConfig: "ascending",
      loaded: false,
    };
    this.handleSelectEntries = this.handleSelectEntries.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.requestSort = this.requestSort.bind(this);
    this.sortTable = this.sortTable.bind(this);
  }

  handleSelectEntries(event) {
    this.setState({ perPage: event.target.value });
  }

  handleFilter(event) {
    let keyword = event.target.value;
    let keywordLowerCase = keyword.toLowerCase();
    getRessource(`api/v1/aliases`).then((response) => {
      let filteredAliases = response.data.data;
      if (keywordLowerCase.length > 2) {
        filteredAliases = filteredAliases.filter(function (obj) {
          return (
            ("alias" in obj &&
              obj["alias"] != null &&
              obj["alias"].toLowerCase().includes(keywordLowerCase)) ||
            ("alias" in obj &&
              obj["url"] != null &&
              obj["url"].toLowerCase().includes(keywordLowerCase))
          );
        });
        this.setState({
          aliases: filteredAliases,
          keyword: keyword,
          total: filteredAliases.length,
        });
      } else {
        this.setState({
          keyword: keyword,
        });
      }
    });
  }

  getAliasesData(currentPage) {
    const { perPage } = this.state;
    let apiAliasURL = `api/v1/aliases?page=${currentPage}&per_page=${perPage}`;
    // filter if owner is set in URL, e.g. /admin/aliases?owner=nicolas.borboen@epfl.ch
    if (this.state.searchOwner) {
      apiAliasURL += `&owner=${this.state.searchOwner}`;
    }
    getRessource(apiAliasURL).then((response) => {
      this.setState({
        aliases: response.data.data,
        total: response.data.meta.total,
        currentPage: response.data.meta.current_page,
        perPage: response.data.meta.per_page,
        loaded: true,
      });
    });
  }

  componentDidMount() {
    this.getAliasesData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.perPage !== prevState.perPage) {
      this.getAliasesData();
    }
    if (this.state.keyword === "" && prevState.keyword !== "") {
      this.getAliasesData();
    }
  }

  requestSort(key) {
    let direction = "ascending";
    if (
      this.state.sortConfig.key === key &&
      this.state.sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }
    this.setState({ sortConfig: { key, direction } });
  }

  sortTable(sortedAliases) {
    const { sortedField, sortConfig } = this.state;
    if (sortedField !== null) {
      sortedAliases.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
  }

  truncate(theStr) {
    return theStr.length > 80 ? theStr.substring(0, 70) + "..." : theStr;
  }

  render() {
    const { aliases, total, currentPage, perPage, loaded } = this.state;
    let content;
    if (aliases.length === 0 && !loaded) {
      content = <Loading />;
    } else {
      let sortedAliases = [...aliases];
      this.sortTable(sortedAliases);
      let pageSubtitleContent = "go.epfl.ch";
      if (this.state.searchOwner) {
        pageSubtitleContent = this.state.searchOwner;
      }
      content = (
        <Fragment>
          <h2 className="tlbx-variant-heading">Aliases</h2>
          <h3 className="tlbx-variant-heading">
            Manage {pageSubtitleContent}'s aliases
          </h3>
          <div
            id="people_wrapper"
            className="dataTables_wrapper dt-bootstrap4 no-footer"
          >
            <div className="row">
              <div className="col-sm-12 col-md-6">
                <div className="dataTables_length" id="people_length">
                  <label>
                    Show{" "}
                    <select
                      name="people_length"
                      aria-controls="people"
                      className="custom-select custom-select-sm form-control form-control-sm"
                      onChange={this.handleSelectEntries}
                    >
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>{" "}
                    entries
                  </label>
                </div>
              </div>
              <div className="col-sm-12 col-md-6">
                <div id="people_filter" className="dataTables_filter">
                  <label>
                    Search:
                    <input
                      type="search"
                      className="form-control form-control-sm"
                      placeholder="Filter by keyword"
                      onChange={this.handleFilter}
                    />
                  </label>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <table
                  id="aliases"
                  className="table dataTableSimple dataTable no-footer"
                >
                  <thead>
                    <tr>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("alias")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>Alias</strong>
                        </button>
                      </th>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("url")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>URL</strong>
                        </button>
                      </th>
                      <th className="no-sort">Owner(s)</th>
                      <th className="no-sort">Hidden</th>
                      <th className="no-sort">Flags</th>
                      <th className="no-sort">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {sortedAliases &&
                      sortedAliases.length > 0 &&
                      sortedAliases.map((alias, index) => (
                        <tr key={index}>
                          <td>
                            <a href={"/" + alias.alias}>{alias.alias}</a>
                          </td>
                          <td>
                            <a href={"/" + alias.alias}>
                              {this.truncate(alias.url)}
                            </a>
                          </td>
                          <td>
                            {alias.owners &&
                              alias.owners.map((owner, index) => {
                                let result = owner;
                                if (alias.owners.length != index + 1) {
                                  result += ", ";
                                }
                                return result;
                              })}
                          </td>
                          <td>
                            <Fragment>
                              {/* { alias.hidden ? 'yes' : 'no' } */}
                              {alias.hidden == true ? (
                                <svg
                                  className="icon feather"
                                  aria-hidden="true"
                                >
                                  <use xlinkHref="#eye-off"></use>
                                </svg>
                              ) : (
                                <svg
                                  className="icon feather"
                                  aria-hidden="true"
                                >
                                  <use xlinkHref="#eye"></use>
                                </svg>
                              )}
                            </Fragment>
                          </td>
                          <td>
                            {alias.flags.length > 0 ? (
                              <span className="badge badge-pill badge-danger">
                                report ({alias.flags.length})
                              </span>
                            ) : (
                              "-"
                            )}
                          </td>
                          <td style={{ textAlign: "center" }}>
                            <a href={"/admin-php/alias/" + alias.alias}>edit</a>{" "}
                            <a href={"/admin-php/alias/confirm-delete/" + alias.alias}>delete</a>
                          </td>
                        </tr>
                      ))}
                    {sortedAliases && sortedAliases.length == 0 && (
                      <tr className="odd">
                        <td
                          valign="top"
                          colSpan="5"
                          className="dataTables_empty"
                        >
                          No matching records found
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-5">
                <div
                  className="dataTables_info"
                  id="people_info"
                  role="status"
                  aria-live="polite"
                >
                  Showing 1 to {perPage} of {total} entries
                </div>
              </div>
              <div className="col-sm-12 col-md-7">
                <div className="dataTables_paginate paging_simple_numbers">
                  <Pagination
                    className="pagination"
                    activePage={currentPage}
                    totalItemsCount={total}
                    itemsCountPerPage={perPage}
                    onChange={(currentPage) => this.getAliasesData(currentPage)}
                    itemClass="paginate_button page-item previous"
                    linkClass="page-link"
                    firstPageText="First"
                    lastPageText="Last"
                  />
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      );
    }
    return content;
  }
}

export { Alias };
