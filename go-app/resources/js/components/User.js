import React, { Fragment, Component } from "react";
import Pagination from "react-js-pagination";
import {
  getRessourceWithToken,
  postRessource,
  getCurrentUser,
} from "../utils/api";
import { AlertSuccess, Loading } from "../utils/Messages";

class User extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      total: 0,
      currentPage: 1,
      perPage: 10,
      keyword: "",
      sortedField: "",
      sortConfig: "ascending",
      generateSuccess: false,
      revokeSuccess: false,
      loaded: false,
    };
    this.handleSelectEntries = this.handleSelectEntries.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleGenerateToken = this.handleGenerateToken.bind(this);
    this.handleDeleteToken = this.handleDeleteToken.bind(this);
    this.requestSort = this.requestSort.bind(this);
    this.sortTable = this.sortTable.bind(this);
  }

  async handleGenerateToken(event) {
    let user_id = event.target.id;
    let currentUser = await getCurrentUser();
    let entrypoint = `api/v1/user/${user_id}/generate-token`;
    postRessource(entrypoint, {}, currentUser.token).then((response) => {
      this.getUsersData();
    });
    this.setState({ generateSuccess: true, revokeSuccess: false });
  }

  async handleDeleteToken(event) {
    let user_id = event.target.id;
    let currentUser = await getCurrentUser();
    let entrypoint = `api/v1/user/${user_id}/delete-token`;
    postRessource(entrypoint, {}, currentUser.token).then((response) => {
      this.getUsersData();
    });
    this.setState({ generateSuccess: false, revokeSuccess: true });
  }

  handleSelectEntries(event) {
    this.setState({ perPage: event.target.value });
  }

  async handleFilter(event) {
    let keyword = event.target.value;
    let keywordLowerCase = keyword.toLowerCase();
    let user = await getCurrentUser();
    getRessourceWithToken(
      `api/v1/users?per_page=${this.state.total}`,
      user.token
    ).then((response) => {
      let filteredUsers = response.data.data;
      if (keywordLowerCase.length > 2) {
        filteredUsers = filteredUsers.filter(function (obj) {
          return (
            "email" in obj &&
            obj["email"] != null &&
            obj["email"].toLowerCase().includes(keywordLowerCase)
          );
        });
        this.setState({
          users: filteredUsers,
          keyword: keyword,
          total: filteredUsers.length,
        });
      } else {
        this.setState({
          keyword: keyword,
        });
      }
    });
  }

  async getUsersData(currentPage = this.state.currentPage) {
    const { perPage } = this.state;
    let user = await getCurrentUser();
    getRessourceWithToken(
      `api/v1/users?page=${currentPage}&per_page=${perPage}`,
      user.token
    ).then((response) => {
      this.setState({
        users: response.data.data,
        total: response.data.meta.total,
        currentPage: response.data.meta.current_page,
        perPage: response.data.meta.per_page,
        loaded: true,
      });
    });
  }

  componentDidMount() {
    this.getUsersData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.perPage !== prevState.perPage) {
      this.getUsersData();
    }
    if (this.state.keyword === "" && prevState.keyword !== "") {
      this.getUsersData();
    }
  }

  updateUserMsg() {
    this.setState({
      generateSuccess: false,
      revokeSuccess: false,
    });
  }

  requestSort(key) {
    let direction = "ascending";
    if (
      this.state.sortConfig.key === key &&
      this.state.sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }
    this.setState({ sortConfig: { key, direction } });
  }

  sortTable(sortedUsers) {
    const { sortedField, sortConfig } = this.state;
    if (sortedField !== null) {
      sortedUsers.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
  }

  render() {
    const { users, total, currentPage, perPage, loaded } = this.state;
    let content;
    if (users.length === 0 && !loaded) {
      content = <Loading />;
    } else {
      let sortedUsers = [...users];
      this.sortTable(sortedUsers);
      content = (
        <Fragment>
          <h2 className="tlbx-variant-heading">Users</h2>
          <h3 className="tlbx-variant-heading">Manage go.epfl.ch's users</h3>
          {this.state.generateSuccess ? (
            <AlertSuccess message={"Successfully generated API token !"} />
          ) : null}
          {this.state.revokeSuccess ? (
            <AlertSuccess message={"Successfully revoked API Token !"} />
          ) : null}
          <div
            id="people_wrapper"
            className="dataTables_wrapper dt-bootstrap4 no-footer"
          >
            <div className="row">
              <div className="col-sm-12 col-md-6">
                <div className="dataTables_length" id="people_length">
                  <label>
                    Show{" "}
                    <select
                      name="people_length"
                      aria-controls="people"
                      className="custom-select custom-select-sm form-control form-control-sm"
                      onChange={(e) => {
                        this.handleSelectEntries(e);
                        this.updateUserMsg();
                      }}
                    >
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>{" "}
                    entries
                  </label>
                </div>
              </div>
              <div className="col-sm-12 col-md-6">
                <div id="people_filter" className="dataTables_filter">
                  <label>
                    Search:
                    <input
                      type="search"
                      className="form-control form-control-sm"
                      placeholder="Filter by keyword"
                      onChange={(e) => {
                        this.handleFilter(e);
                        this.updateUserMsg();
                      }}
                    />
                  </label>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <table id="users" className="table table-boxed NodataTable">
                  <thead>
                    <tr>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("is_admin")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>
                            <svg className="icon feather" aria-hidden="true">
                              <use xlinkHref="#user"></use>
                            </svg>
                          </strong>
                        </button>
                      </th>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("sciper")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>Sciper</strong>
                        </button>
                      </th>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("firstname")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>Firstname</strong>
                        </button>
                      </th>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("lastname")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>Lastname</strong>
                        </button>
                      </th>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("email")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>Email</strong>
                        </button>
                      </th>
                      <th>
                        <button
                          type="button"
                          onClick={() => this.requestSort("aliases_count")}
                          style={{
                            background: "transparent",
                            border: "none",
                            outline: "none",
                          }}
                        >
                          <strong>Aliases count</strong>
                        </button>
                      </th>
                      <th className="no-sort">API Token</th>
                    </tr>
                  </thead>
                  <tbody>
                    {sortedUsers &&
                      sortedUsers.length > 0 &&
                      sortedUsers.map((user, index) => (
                        <tr key={index}>
                          <td>
                            <Fragment>
                              {/* {user.is_admin ? "yes" : "no"} */}
                              {user.is_admin == true ? (
                                <svg
                                  className="icon feather"
                                  aria-hidden="true"
                                  style={{stroke:'red'}}
                                >
                                  <use xlinkHref="#user-check"></use>
                                </svg>
                              ) : (
                                <svg
                                  className="icon feather"
                                  aria-hidden="true"
                                >
                                  <use xlinkHref="#user"></use>
                                </svg>
                              )}
                            </Fragment>
                          </td>
                          <td>
                            <Fragment>
                              <a href={"https://people.epfl.ch/" + user.sciper}>
                                {user.sciper}
                              </a>
                            </Fragment>
                          </td>
                          <td>{user.firstname}</td>
                          <td>{user.lastname}</td>
                          <td>{user.email}</td>
                          <td>
                            <Fragment>
                              <a href={"/admin/aliases?owner=" + user.email}>
                                {user.aliases_count}
                              </a>
                            </Fragment>
                          </td>
                          <td style={{ textAlign: "center" }}>
                            {user.has_api_token ? (
                              <Fragment>
                                <a
                                  id={user.id}
                                  onClick={this.handleGenerateToken}
                                  href="#"
                                >
                                  regenerate
                                </a>{" "}
                                <a
                                  id={user.id}
                                  onClick={this.handleDeleteToken}
                                  href="#"
                                >
                                  revoke
                                </a>
                              </Fragment>
                            ) : (
                              <a
                                id={user.id}
                                onClick={this.handleGenerateToken}
                                href="#"
                              >
                                generate
                              </a>
                            )}
                          </td>
                        </tr>
                      ))}
                    {sortedUsers && sortedUsers.length == 0 && (
                      <tr className="odd">
                        <td
                          valign="top"
                          colSpan="3"
                          className="dataTables_empty"
                        >
                          No matching records found
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-5">
                <div className="dataTables_info" id="people_info" role="status">
                  Showing 1 to {perPage} of {total} entries
                </div>
              </div>
              <div className="col-sm-12 col-md-7">
                <div className="dataTables_paginate paging_simple_numbers">
                  <Pagination
                    className="pagination"
                    activePage={currentPage}
                    totalItemsCount={total}
                    itemsCountPerPage={perPage}
                    onChange={(currentPage) => {
                      this.getUsersData(currentPage);
                      this.updateUserMsg();
                    }}
                    itemClass="paginate_button page-item previous"
                    linkClass="page-link"
                    firstPageText="First"
                    lastPageText="Last"
                  />
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      );
    }
    return content;
  }
}

export { User };
