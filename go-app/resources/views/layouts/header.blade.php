{{-- From EPFL https://epfl-si.github.io/elements/#/organisms/header/light-fr/full --}}
<header role="banner" class="header header-light navbar-laravel" id="app-layout">
  <div class="drawer mr-3 mr-xl-0">
    <button class="drawer-toggle">
      <svg class="icon" aria-hidden="true"><use xlink:href="#icon-chevron-right"></use></svg>
    </button>
    <a href="//www.epfl.ch" class="drawer-link">
      <span class="text">
        Go to EPFL main site
      </span>
    </a>
  </div>

  <div class="header-light-content">
    <a class="logo" href="/">
      <img src="/svg/epfl-logo.svg" alt="Logo EPFL, École polytechnique fédérale de Lausanne" class="img-fluid">
    </a>
    <h1><acronyme title="EPFL URL Shortener">{{ config('app.name') }}</acronyme></h1>

    @include('layouts.menu.menu')

    @include('layouts.menu.mobile.search')
    @include('layouts.menu.partials.search')

    @include('layouts.menu.partials.user')

    @include('layouts.menu.mobile.burger')

  </div>
  @include('layouts.partials.ribbon')
</header>
