@if ($errors->any())
  <div id="errors">
    @foreach ($errors->all() as $error)
      <div class="error_message alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Watch out !</strong> {{{$error}}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endforeach
  </div>
@endif

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
