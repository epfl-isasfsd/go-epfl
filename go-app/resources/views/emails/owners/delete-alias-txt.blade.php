{{-- Email template for deletion to owners (TXT) --}}
@if(count($alias->owners) > 1)
Dear owners,
@else
Dear owner,
@endif
On {{ date('Y-m-d H:i:s') }}, {{$user->firstname}} {{$user->lastname}} (<https://people.epfl.ch/{{$user->sciper}}>)
has deleted the alias *{{$alias->alias}}* which was redirecting to *{{$alias->url->url}}*.

The alias *{{ $alias->alias }}* was created on {{ $alias->url->created_at }}
and had been clicked {{ $alias->clicks_count }} times.

This alias was owned by
@foreach ($alias->owners as $owner)
  • {{ $owner->firstname }} {{ $owner->lastname }} <{{ $owner->email }}>
@endforeach

Feel free to use the <{{ url('/contact') }}> page if you have questions.

Best regards,
the GoEPFL system
