{{-- Email template for deletion to owners (HTML) --}}
@if(count($alias->owners) > 1)
Dear owners,<br />
@else
Dear owner,<br />
@endif
<br />
On {{ date('Y-m-d H:i:s') }}, <a href="https://people.epfl.ch/{{$user->sciper}}">{{ $user->firstname }} {{ $user->lastname }}</a> has deleted
the alias <b>{{ $alias->alias }}</b> which was redirecting to <a href="{{ $alias->url->url }}">{{ $alias->url->url }}</a>.<br />
<br />
The alias <b>{{ $alias->alias }}</b> was created on {{ $alias->url->created_at }}
and had been clicked {{ $alias->clicks_count }} times.<br />
<br />
This alias was owned by
<ul>
@foreach ($alias->owners as $owner)
  <li><a href="mailto:{{ $owner->email }}">{{ $owner->firstname }} {{ $owner->lastname }}</a></li>
@endforeach
</ul>
<br />
<br />
Feel free to use the <a href="{{ url('/contact') }}">contact</a> page if you have questions.<br /><br />
Best regards,<br />
the GoEPFL system

@include('emails.footer')
