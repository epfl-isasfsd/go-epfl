Dear admins,<br/>
<br/>
On {{$alias->updated_at}}, {{$user->firstname}} {{$user->lastname}} has updated<br/>
the alias <b>{{$alias->alias}}</b> for the url <a href="{{$alias->url->url}}">{{$alias->url->url}}</a>.<br/>
<br/>
Firstname: {{$user->firstname}}<br/>
Lastname: {{$user->lastname}}<br/>
Sciper: {{$user->sciper}} (<a href="https://people.epfl.ch/{{$user->sciper}}">https://people.epfl.ch/{{$user->sciper}}</a>)<br/>
Username: {{$user->username}}<br/>
Email: <a href="mailto:{{$user->email}}">{{$user->email}}</a><br/>
<br/>
URL: <a href="{{config('app.url')}}{{$alias->alias}}">{{$alias->url->url}}</a> (Updated on {{$alias->url->updated_at}})<br/>
Alias: <b>{{$alias->alias}}</b><br/>
Go: <a href="{{config('app.url')}}{{$alias->alias}}">{{config('app.url')}}{{$alias->alias}}</a> → <a href="{{$alias->url->url}}">{{$alias->url->url}}</a><br/>
Hidden: {{$alias->hidden ? 'yes' : 'no'}}<br/>
Obsolescence: {{$alias->obsolescence_date ?: '∞'}}<br/>
<br/>
Reveal: <a href="{{config('app.url')}}reveal/{{$alias->alias}}">{{config('app.url')}}reveal/{{$alias->alias}}</a><br/>
Report: <a href="{{config('app.url')}}report/{{$alias->alias}}">{{config('app.url')}}report/{{$alias->alias}}</a><br/>
Edit: <a href="{{config('app.url')}}edit/alias/{{$alias->alias}}">{{config('app.url')}}edit/alias/{{$alias->alias}}</a><br/>
Info: <a href="{{config('app.url')}}info/{{$alias->alias}}">{{config('app.url')}}info/{{$alias->alias}}</a><br/>
<br/>
@include('emails.footer')
