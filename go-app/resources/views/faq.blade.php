@extends('layouts.app')

@section('content')
  <h3 class="tlbx-variant-heading">Usage {{$page}}</h3>

  <button class="collapse-title collapse-title-desktop" type="button" data-toggle="collapse" data-target="#I-have-question-or-problem" aria-expanded="true" aria-controls="I-have-question-or-problem">
    I have question or problem related to {{ config('app.name') }}...
  </button>
  <div class="collapse collapse-item collapse-item-desktop show" id="I-have-question-or-problem">
    Good that you checked this FAQ! If you can't find an answer here, please have a look to the
    <a href="https://support.epfl.ch/kb_view.do?sys_kb_id=4b17ae16dbbffb041f32c14c2296191f">KB0015277</a>.
    The <a href="https://support.epfl.ch/epfl?id=epfl_service_status&service=790bc37edba788dc9920898532961935">status page</a> could indicate a maintenance or an outage.<br /><br />
    You can use this <a href="https://support.epfl.ch/epfl?id=epfl_sc_cat_item&sys_id=1b7123e14f1d4300fe35adee0310c7f0&service_id=7c6347eadb6fc0dc992089853296199e">form</a> to create a support request directly in ServiceNow.<br /><br />
    Last but not least, the site has a <a href="{{ config('app.url') }}contact">contact form</a> in which you can either contact the administrator or create an issue on the <a href="https://gitlab.com/epfl-isasfsd/go-epfl/-/issues">GitLab</a> repository.<br /><br />
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Can-an-URL-have-different-alias" aria-expanded="false" aria-controls="Can-an-URL-have-different-alias">
    Can an URL have different alias?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Can-an-URL-have-different-alias">
    <p>
      Yes. An URL can have from 1 to N aliases.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Can-an-alias-have-different-owners" aria-expanded="false" aria-controls="Can-an-alias-have-different-owners">
    Can an alias have different owners?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Can-an-alias-have-different-owners">
    <p>
      Yes. An alias can have from 0 to N owners.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-is-a-hidden-alias" aria-expanded="false" aria-controls="What-is-a-hidden-alias">
    What is a hidden alias?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-is-a-hidden-alias">
    <p>
      A hidden alias is an alias which won't be listed in the <a href="{{ config('app.url') }}browse">browse</a> page, in the <a href="{{ config('app.url') }}feed">Atom feed</a> nor in the <a href="https://t.me/joinchat/T2FsT1X3IpGBS11z">Telegram Channel</a>.<br />
      Please note that it isn't private, meaning that people may still be able to access it.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-is-TTL" aria-expanded="false" aria-controls="What-is-TTL">
    What is TTL?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-is-TTL">
    <p>
      It means "Time To Live", in other terms, expiration date.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#How-can-I-preview-a-link" aria-expanded="false" aria-controls="How-can-I-preview-a-link">
    How can I preview a link?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="How-can-I-preview-a-link">
    <p>
      If you want to know where a shortened URL will redirect, you can find it out with the URL <code>/reveal/{alias}</code>.
      For instance, to know where <a href="{{ config('app.url') }}mag">{{ config('app.url') }}mag</a> will redirect, use <a href="{{ config('app.url') }}reveal/mag">{{ config('app.url') }}reveal/mag</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#How-can-I-report-a-link" aria-expanded="false" aria-controls="How-can-I-report-a-link">
    How can I report a link?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="How-can-I-report-a-link">
    <p>
      The report feature is very succint for now, but if you click the report link, administrators will reveice a notification. When a link has been reported enough times,
      it won't be accessible directly and users will be headed to the <code>reveal</code> page, letting the choice to continue or not.<br /><br />
      Please note that in case of phishing or malevolent links, <a href="{{ config('app.url') }}contact">contacting the administrator</a> is the best move.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Is-there-some-magic-URLs" aria-expanded="false" aria-controls="Is-there-some-magic-URLs">
    Is there some magic URLs?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Is-there-some-magic-URLs">
    <p>
      Yes. <code>/INC....</code>, <code>/KB....</code>, <code>/CHG....</code>,
      <code>/TASK....</code>, <code>/CIM....</code>, <code>/CTASK....</code>,
      <code>/OUT....</code>, <code>/RITM....</code>, <code>/IDEA....</code>,
      <code>/PRB....</code>,<code>/PRJ....</code>, <code>/SVC....</code>,
      <code>/RSK....</code> and <code>/AUD....</code>.
      will respectively redirect to the relevant ServiceNow page. It's a quick
      and efficient way to access and share these ServiceNow items.
      <br>
      <br>
      There's also the Moodle magic redirects. For example,
      <a href="https://go.epfl.ch/CS-101">https://go.epfl.ch/CS-101</a>
      will redirect to the corresponding
      <a href="https://go.epfl.ch/CS-101">moodle.epfl.ch</a> URL.
      <br>
      <br>
      <a href="https://go.epfl.ch/me">go.epfl.ch/me</a> redirects you to your
      <a href="https://go.epfl.ch/me">people</a> page.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Can-I-have-stats" aria-expanded="false" aria-controls="Can-I-have-stats">
    Can I have usage statistics for my alias?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Can-I-have-stats">
    <p>
      Yes, each alias have an info page: <code>/info/{alias}</code>.
      Access the <a href="{{ config('app.url') }}mag">{{ config('app.url') }}mag</a> alias info with <a href="{{ config('app.url') }}info/mag">{{ config('app.url') }}info/mag</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#QR-code-of-my-alias" aria-expanded="false" aria-controls="QR-code-of-my-alias">
    QR code of my URL?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="QR-code-of-my-alias">
    <p>
      A <a href="https://en.wikipedia.org/wiki/QR_code">QR Code</a> is avalaible on the info page. Head to  <code>/info/{alias}</code> to get it.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Can-I-use-Emoji-in-alias-or-url" aria-expanded="false" aria-controls="Can-I-use-Emoji-in-alias-or-url">
    Emoji or accented char in alias or URL?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Can-I-use-Emoji-in-alias-or-url">
    <p>
      Nah, right now this is not possible.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Are-other-protocole-allowed" aria-expanded="false" aria-controls="Are-other-protocole-allowed">
    Does my URL have to start with "http"?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Are-other-protocole-allowed">
    <p>
      No, some other protocols are working. <code>ftp://ftp.archive.ubuntu.com/</code> will work, <code>spotify:artist:6gK1Uct5FEdaUWRWpU4Cl2</code> won't.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Is-there-an-API" aria-expanded="false" aria-controls="Is-there-an-API">
    Is there an API?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Is-there-an-API">
    <p>
      Yes, {{ config('app.name') }} provides an API: <a href="{{ config('app.url') }}api">{{ config('app.url') }}api</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Shorten-links-while-browsing" aria-expanded="false" aria-controls="Shorten-links-while-browsing">
    How can I shorten links while browsing (webextension)?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Shorten-links-while-browsing">
    <p>
      If you are using <a href="http://getfirefox.com/">Firefox</a>, you can install the <a href="{{ config('app.url') }}extension">webextension</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Are-go-cut-and-short-the-same" aria-expanded="false" aria-controls="Are-go-cut-and-short-the-same">
    Are {{ config('app.name') }}, cut.epfl.ch and short.epfl.ch the same ?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Are-go-cut-and-short-the-same">
    <p>
      Yes they are sharing the same database, but {{ config('app.name') }} is a 1 char shorter than cut.epfl.ch and 2 chars shorter than short.epfl.ch...<br />
      Since {{ config('app.name') }} has been rewritten, cut.epfl.ch and short.epfl.ch are redirection to {{ config('app.name') }}. Feel free to use them!
    </p>
  </div>


  <p></p>
  <h3 class="tlbx-variant-heading">General {{$page}}</h3>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Is-it-any-good" aria-expanded="false" aria-controls="Is-it-any-good">
    Is it any good?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Is-it-any-good">
    <p>
      <a href="https://news.ycombinator.com/item?id=3067434">Yes</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-is-URL-shortening" aria-expanded="false" aria-controls="What-is-URL-shortening">
    What is URL shortening?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-is-URL-shortening">
    <p>
      URL shortening is a technique in which a URL may be made substantially
      shorter in length and have more meaning.<br />
      Example:
      <a href="{{ config('app.url') }}mag">https://www.epfl.ch/campus/services/communication/en/audiences-and-channels/epfl-magazine/</a>
      becomes
      <a href="{{ config('app.url') }}mag">{{ config('app.url') }}mag</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-is-the-purpose-of-this-site" aria-expanded="false" aria-controls="What-is-the-purpose-of-this-site">
    What is the purpose of this site?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-is-the-purpose-of-this-site">
    <p>
      Besides from the URL shortening this site has other goals:<br />
      <ul>
        <li>A way to easily remember all complicated URLs</li>
        <li>To have a directory of EPFL's links</li>
        <li>To have a quick and safe (and and corporate compliant) way to shorten URLs</li>
      </ul>
      If you read french, have a look to this <a href="{{ config('app.url') }}gofi">article</a> in the Flash Informatique.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#I-like-your-logos" aria-expanded="false" aria-controls="I-like-your-logos">
    I like your logos!
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="I-like-your-logos">
    <p>
      <a href="{{ config('app.url') }}logo">
          <img src="/logo/GoEPFL_red.svg" width="55px" style="float: right; padding: 1px;" />
      </a>
      We love them too!<br/>
      In mid-2021, Tania Di Paola and Titouan Veuillet, two Interactive Media
      Designer (IMD) apprentices worked on a new logo. The result of their
      work is available on the <a href="{{ config('app.url') }}design">design</a>
      page.    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Is-go-epfl-ch-an-official-EPFL-service" aria-expanded="false" aria-controls="Is-go-epfl-ch-an-official-EPFL-service">
    Is {{ config('app.name') }} an official EPFL service?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Is-go-epfl-ch-an-official-EPFL-service">
    <p>
      Yes! Since march 2020 <a href="{{ config('app.url') }}">{{ config('app.name') }}</a> is an official EPFL service.<br />
      It means that it has:
      <ul>
        <li>a name (SVC1207),</li>
        <li>a knowledge base (<a href="https://support.epfl.ch/kb_view.do?sys_kb_id=4b17ae16dbbffb041f32c14c2296191f">KB0015277</a>),</li>
        <li>a status page (<a href="https://support.epfl.ch/epfl?id=epfl_service_status&service=790bc37edba788dc9920898532961935">epfl_service_status</a>),</li>
        <li>and ticket queue (<a href="https://support.epfl.ch/epfl?id=epfl_sc_cat_item&sys_id=1b7123e14f1d4300fe35adee0310c7f0&service_id=7c6347eadb6fc0dc992089853296199e">create one</a>).</li>
      </ul>
      More documents about this EPFL service can be found on <a href="https://confluence.epfl.ch:8443/x/YYEaBw">confluence</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#What-system-is-used" aria-expanded="false" aria-controls="What-system-is-used">
    What system is used? #techstack
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="What-system-is-used">
    <p>
      The first version of <a href="{{ config('app.url') }}">{{ config('app.name') }}</a> was a mix between PHURL (https://code.google.com/archive/p/phurl/) with some parts of YOURLS (http://yourls.org/).
    </p>
    <p>
      Early 2019, Loïc Humbert (an IT apprentice) was asked to refresh the whole system during his end of apprentiship work (called TPI - Travail Pratique Individuel).
      While still running some parts of YOURLS, the website has been rewritten to the PHP Framework <a href="https://laravel.com/">Laravel</a>.
      The database running <a href="{{ config('app.url') }}">{{ config('app.name') }}</a> is <a href="PostgreSQL">PostgreSQL</a>.
      The whole system is running in <a href="https://www.docker.com/resources/what-container">containers</a>.
      The webserver is <a href="https://www.nginx.com/">NGINX</a> behind <a href="Traefik">Træfik</a>.<br />
      You may be interessed to have a look to the <a href="https://gitlab.com/epfl-isasfsd/go-epfl">source code</a>.
    </p>
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Changelog-and-Roadmap" aria-expanded="false" aria-controls="Changelog-and-Roadmap">
    Is there a changelog and a roadmap?
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Changelog-and-Roadmap">
    No, but that would be a good idea. The closest think to them is the <a href="https://gitlab.com/epfl-isasfsd/go-epfl/-/boards">issues board</a> on GitLab.
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#Feature-request-issues" aria-expanded="false" aria-controls="Feature-request-issues">
    Feature request, issues, etc.
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="Feature-request-issues">
    @include('partials.issues-and-contact')
  </div>

  <button class="collapse-title collapse-title-desktop collapsed" type="button" data-toggle="collapse" data-target="#I-would-like-to-contribute" aria-expanded="false" aria-controls="I-would-like-to-contribute">
    I would like to contribute!
  </button>
  <div class="collapse collapse-item collapse-item-desktop" id="I-would-like-to-contribute">
    That would be awesome! ❤️<br /><br />
    Have a look to the <a href="https://gitlab.com/epfl-isasfsd/go-epfl">source code</a> and already listed <a href="https://gitlab.com/epfl-isasfsd/go-epfl/-/issues">issues</a>.<br /><br />
    We want to let you know that we are here to help, feel free to ask some using the <a href="{{ config('app.url') }}contact">contact form</a>.<br /><br />
  </div>

<p></p>
<p></p>

@endsection
