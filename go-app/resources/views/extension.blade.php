@extends('layouts.app')

@section('content')
    <h3 class="tlbx-variant-heading">Web Extension</h3>
    <br>
    <h4>Firefox</h4>
    <p>
        {{ config('app.name') }} has now its own Firefox extension with the intention of facilitate the creation of
        short links while browsing.
        <br>
        Please follow the <a href="https://gitlab.com/epfl-isasfsd/go-epfl-webextension#installation">installation process</a>
        of the <a href="https://gitlab.com/epfl-isasfsd/go-epfl-webextension">webextension GitLab repository</a>.
    </p>
    <h5>Preview</h5>
    <p>
      <img src="/img/go-webextension.png" alt="Web Extension preview" style=" border: 1px solid grey;padding: 20px;">
    </p>
@endsection
