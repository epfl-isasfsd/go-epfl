@extends('admin.layout')

@section('admin-content')
    <h3 class="tlbx-variant-heading">Manage users</h3>
    <br />
    <table id="people" class="table dataTable table-boxed" >
        <thead>
            <tr>
                <th>Email</th>
                <th>Aliases count</th>
                <th class="no-sort">API Token</th>
            </tr>
        </thead>
        <tbody>
        @foreach($people as $user)
            <tr>
                <td>{{$user->email}}</td>
                <td>{{$user->aliases_count}}</td>
                <td style="text-align: center">
                    @isset($user->api_token)
                        <a href="#">revoke</a>
                        <a href="#">regenerate</a>
                    @else
                        <a href="#">generate</a>
                    @endisset
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
