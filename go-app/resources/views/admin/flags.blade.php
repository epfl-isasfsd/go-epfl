@extends('admin.layout')

@section('admin-content')
    <h3 class="tlbx-variant-heading">Manage flags 🚩</h3>
    <br />
    <div class="row">
        <div class="col-xl-12">
            <br />
            <h4>Reports</h4>
            <table class="table dataTableSimple">
                <thead>
                    <tr>
                        <th>URL</th>
                        <th>Alias</th>
                        <th>Reports count</th>
                        <th class="no-sort">Edit</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($reports as $alias => $details)
                    <tr>
                        <td><a href="{{url($alias)}}">{{$details['url']['url']}}</a></td>
                        <td><a href="{{url($alias)}}">{{$alias}}</a></td>
                        <td>{{$details['count']}} 🚩</td>
                        <td>
                            <a href="/admin/alias/{{$alias}}">edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <br />
            <h4>Unreliable</h4>
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th>URL</th>
                        <th>Alias-es</th>
                        <th>Status</th>
                        <th class="no-sort">Edit</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($unaccessible as $url => $unreliable)
                    <tr>
                        <td><a href="{{$url}}">{{$url}}</a></td>
                        <td>
                            @if (count($unreliable['alias']))
                                @foreach($unreliable['alias'] as $alias)
                                    <a href="{{url($alias->alias)}}">{{ $alias->alias }}</a>
                                @endforeach
                            @else
                                -
                            @endif
                        </td>
                        <td><span title="{{$unreliable['flag']->details['message']}}">{{$unreliable['flag']->details['status_code']}}</span></td>
                        <td>
                            @if (count($unreliable['alias']))
                                @foreach($unreliable['alias'] as $alias)
                                    <a href="{{url('/admin/alias/'.$alias->alias)}}">{{ $alias->alias }}</a>
                                @endforeach
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <br />
            <h4>Blacklist</h4>
            <table class="table">
                <tbody>
                @foreach($blacklist as $object => $blacklist_count)
                    <tr>
                        <td>
                            {{$object}} ({{$blacklist_count}})
                        </td>
                        <td><a href="/blacklist/{{$blacklisted->id}}">details</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
