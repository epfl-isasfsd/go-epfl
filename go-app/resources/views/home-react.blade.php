@extends('layouts.app-react')
@section('content')
    <h3 class="tlbx-variant-heading">GO EPFL - URL shortener</h3>

    @if(!env("APP_DEGRADED_MODE"))
      @auth
      <br />

      <h5>Shorten a new URL</h5>

      @include('partials.edit_alias', [
          'prev_url'      => old('url') ?? '',
          'prev_alias'    => old('alias') ?? '',
          'hidden'        => false,
          'ttl'           => 0,
          'submitButton'  => 'Shorten URL',
          'sticky_inputs' => 'new'
      ])
      @endauth

      @guest
      <br />
      <h3>Info</h3>
      <p>
          <a href="{{ config('app.url') }}">{{ config('app.name') }}</a> is a service offered to the
          EPFL community to shorten URLs.
          <br />
          <br />
          <a href="/login" class="btn btn-info" role="button">Login</a> is required in order to create new shortened URLs,
          but other pages of this website (<a href="{{ route('about') }}">{{ __('About') }}</a>,
          <a href="{{ route('stats') }}">{{ __('Stats') }}</a>, <a href="{{ route('FAQ') }}">{{ __('FAQ') }}</a>, ...)
          can be reached as an unauthenticated user.
      </p>

      <br />
      <h3>What is it?</h3>
      <p>
          URL shortening is a technique in which a URL may be made substantially
          shorter in length and have (sometimes) more meaning.<br />
          <h5>Example:</h5>
          <a href="{{ config('app.url') }}mag">https://www.epfl.ch/campus/services/communication/audiences-canaux/epfl-magazine/</a> becomes <a href="{{ config('app.url') }}mag">{{ config('app.url') }}mag</a>.
      </p>

      <br />
      <h3>Reveal URLs</h3>
      <p>
          If you want to know where a shortened URL will redirect, you can find it out with the URL <code>/reveal/alias</code>.
          <br />For instance, to know where <a href="{{ config('app.url') }}mag">{{ config('app.url') }}<b>mag</b></a> will redirect, use <a href="{{ config('app.url') }}reveal/mag">{{ config('app.url') }}<b>reveal</b>/mag</a>.
      </p>
      @endguest

    @else
      @include('layouts.partials.degradedmode')
    @endif
@endsection
