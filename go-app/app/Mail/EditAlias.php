<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EditAlias extends Mailable
{
    use Queueable, SerializesModels;

    public $alias;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($alias, $user)
    {
        $this->alias = $alias;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.edit-alias')
                    ->text('emails.edit-alias-txt')
                    ->subject(config('mail.prefix') . ' Alias updated (' . $this->alias->alias . ')')
                    ->with([
                        'user'  => $this->user,
                        'alias' => $this->alias
                    ]);
    }
}
