<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'sciper' => $this->sciper,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'aliases_count' => $this->aliases_count,
            'has_api_token' => $this->api_token != "",
            'is_admin' => $this->is_admin,
        ];
    }
}
