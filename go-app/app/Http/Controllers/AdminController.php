<?php

namespace App\Http\Controllers;

use App\Models\Url;
use App\Models\User;
use App\Models\Flag;
use App\Models\Alias;
use App\Mail\DeleteAlias;
use App\Models\BlacklistItem;
use App\Traits\Pagination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

use App\Helpers\Telegram;

class AdminController extends Controller
{
    use Pagination;

    public function index()
    {
        return view('admin.home', ['page' => 'Admin', 'sub' => 'none']);
    }

    public function aliases()
    {
        $aliases = Alias::with(['url', 'owners', 'flags']);
        $owners = array();
        $flags = array();
        foreach ($aliases->get() as $alias)
        {
            foreach ($alias->owners as $owner)
                $owners[$alias->alias][] = $owner->email;
            foreach ($alias->flags as $flag)
                $this->increment($flags[$alias->alias], $flag->flag_type);

        }
        $aliases = $aliases->paginate();
        return view('admin.aliases', [
            'page' => 'Admin',
            'sub'  => 'Aliases',
            'displayed_pages' => $this->pagination($aliases),
            'aliases' => $aliases,
            'owners' => $owners,
            'flags' => $flags
        ]);
    }

    public function editAlias(Alias $alias)
    {
        $owners = array();
        foreach ($alias->owners as $owner)
            $owners []= $owner->email;

        return view('admin.edit_alias', [
            'page'              => 'Admin',
            'sub'               => 'Aliases',
            'alias'             => $alias->alias,
            'url'               => $alias->url->url,
            'obsolescence_date' => $alias->obsolescence_date,
            'ttl'               => $alias->obsolescence_date ?: 0,
            'hidden'            => $alias->hidden,
            'owners'            => $owners
        ]);
    }

    public function updateAlias(Request $request)
    {
        $old_alias = $request->input('original_alias');
        $alias = Alias::where("alias", $old_alias)->first();

        if ($alias->is(new Alias()))
            abort(404);

        $hiddenAlias = $request->has('hidden-update-admin');
        $alias->hidden = $hiddenAlias;
        $alias->alias = $request->input('alias-update-admin');
        $timeToLive = $request->input('ttl-update-admin');
        if ($timeToLive > 0) {
            $alias->obsolescence_date = date('Y-m-d', strtotime('+'.$timeToLive.' months'));
        } else {
            $alias->obsolescence_date = null;
        }

        $url = Url::firstOrCreate(['url' => $request->input('url-update-admin')]);
        $alias->url()->associate($url);

        $validator = $alias->validate();
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $alias->save();

        return redirect('admin-php/alias/' . $alias->alias)->with('status', 'The alias has been updated');
    }

    public function confirmDelete($alias)
    {
        $alias = Alias
                ::where(['alias' => $alias])
                ->withCount('clicks')
                ->first();

        if ($alias->is(new Alias()))
            abort(404);
        $obsolescence_date = new \DateTime($alias->obsolescence_date);


        return Auth::user()->is_admin ? view('alias.confirm-alias-delete', [
            'aliasObj'          => $alias,
            'alias'             => $alias->alias,
            'url'               => $alias->url->url,
            'hidden'            => $alias->hidden,
            'obsolescence_date' => $obsolescence_date->format('Y-m-d'),
            'ttl'               => $alias->obsolescence_date ?: null,
            'numClicks'         => $alias->clicks_count,
            'owners'            => $alias->owners,
        ]) : abort(403, 'You are not allowed to delete this alias');
    }

    public function deleteAlias($alias)
    {
        $alias = Alias
                ::where(['alias' => $alias])
                ->withCount('clicks')
                ->first();

        Mail::to(config('mail.admins'))
            ->send(new DeleteAlias($alias, Auth::user(), "admins"));

        $ownersArray = [];
        foreach ($alias->owners as $key => $value) {
            if ($value->email != Auth::user()->email) {
                array_push($ownersArray, $value->email);
            }
        }

        Mail::to(Auth::user()->email)
            ->cc($ownersArray)
            ->send(new DeleteAlias($alias, Auth::user(), "owners"));

        $tg = new Telegram;
        $tg->aliasDeleted($alias, Auth::user());

        if ($alias->is(new Alias()))
            abort(404);
        if (Auth::user()->is_admin) {
            $success_message = "Alias \"$alias->alias\" successfully deleted.";
            // need to remove the fk permissions first...
            \DB::table('permissions')
                ->where('alias_id', $alias->id)
                ->delete();

            \DB::table('flags')
                ->where('object_id', $alias->id)
                ->where('object_type', 'App\\Models\\Alias')
                ->delete();

            $alias->delete();
            return redirect('/admin-php/aliases')->with('status', "Alias \"$alias->alias\" successfully deleted.");
        } else {
            return 403;
        }
    }

    public function updateOwner(Request $request)
    {
        $owner_email = $request->input('new_owner');
        $alias = Alias::where('alias', $request->input('alias'))->first();
        if (is_null($user = User::where('email', $owner_email)->first()))
            return redirect()->back()->withErrors("The user with email \"$owner_email\" does not exist");
        $alias->owners()->attach($user);
        $alias->save();
        return redirect()->back()->with('status', "$user->username has been added to $alias->alias's owners");
    }

    public function deleteOwner(Request $request)
    {
        $alias = Alias::where('alias', $request->input('alias'))->first();
        $user = User::where('email', $request->input('owner'))->first();
        $alias->owners()->detach($user);
        $alias->save();
        return redirect()->back()->with('status', "$user->username has been removed from $alias->alias's owners");
    }

    public function flags()
    {
        $_reports = Flag::with('object')
            ->where('flag_type', 'report')->get();
        $reports = array();
        foreach ($_reports as $report) {
            if (!empty($report->object)) {
                if (!isset($reports[$report->object['alias']])) {
                    $reports[$report->object['alias']]['count'] = 0;
                    $reports[$report->object['alias']]['url'] = '';
                }
                $reports[$report->object['alias']]['count'] += 1;
                $reports[$report->object['alias']]['url'] = Url::where('id', $report->object['url_id'])->first();
            }
        }

        $_unaccessible = Flag::with('object')
            ->where('flag_type', 'reliability')->get();
        $unaccessible = array();
        foreach ($_unaccessible as $unreliable) {
            if (!empty($unreliable->object)) {
                $test = [];
                if (count($unreliable->object->aliases)) {
                    foreach ($unreliable->object->aliases as $alias) {
                        array_push($test, $alias);
                    }
                }
                $unaccessible[$unreliable->object['url']]['alias'] = $test;
                $unaccessible[$unreliable->object['url']]['flag'] = $unreliable;
            }
        }

        $_blacklist = Flag::with('object')
            ->where('flag_type', 'blacklist')->get();

        $blacklist = array();
        foreach ($_blacklist as $item)
            if (!empty($item->object))
                if (isset($item->object['url']))
                    $this->increment($blacklist, $item->object['url']);
                elseif (isset($item->object['alias']))
                    $this->increment($blacklist, $item->object['alias']);

        return view('admin.flags', [
            'page'          => 'Admin',
            'sub'           => 'Flags',
            'reports'       => $reports,
            'unaccessible'  => $unaccessible,
            'blacklist'     => $blacklist,
        ]);
    }

    private function increment(&$array, $key)
    {
        if(!isset($array[$key]))
            $array[$key] = 1;
        else
            $array[$key] += 1;
    }

    public function people()
    {
        $people = User::withCount(['aliases'])->get();
        return view('admin.people', [
            'page' => 'Admin',
            'sub'  => 'People',
            'people' => $people,
        ]);
    }

    public function blacklist()
    {
        $blacklist = BlacklistItem::pluck('keyword')->all();
        return view('admin.blacklist', [
            'page' => 'Admin',
            'sub'  => 'Blacklist',
            'blacklist' => $blacklist
        ]);
    }

    public function appendToBlacklist(Request $request)
    {
        $validated = $request->validate([
            'keyword' => 'required|unique:blacklist_items|max:255'
        ]);
        $word = new BlacklistItem();
        $word->keyword = $validated['keyword'];
        $word->save();
        return redirect()->back()
                         ->with('status', "Keyword \"$word->keyword\" successfully added to blacklist");
    }

    public function deleteFromBlacklist(BlacklistItem $keyword)
    {
        $success_message = "Keyword \"$keyword->keyword\" successfully deleted from blacklist";
        $keyword->delete();
        Session::flash('status', $success_message);
        return $success_message;
    }

    public function advanced()
    {
        $administrators = User::admin()->get();
        return view('admin.advanced', [
            'page' => 'Admin',
            'sub'  => 'Advanced',
            'administrators' => $administrators
        ]);
    }

    public function addAdministrator(Request $request)
    {
        $new_admin_email = $request->input('new_admin');
        $new_admin       = User::where('email', $new_admin_email)->first();
        if (empty($new_admin))
            return redirect()->back()->withErrors(
                "There is no user with the email address \"$new_admin_email\""
            );
        $new_admin->is_admin = true;
        $new_admin->save();
        return redirect()->back()->with('status', "$new_admin->username is now administrator");
    }

    public function runReliabilityTest()
    {
        dispatch(function () {
            $reliability_test = new \App\Console\ReliabilityTest;
            $reliability_test();
        })->onQueue('reliability_test');
        return redirect()->back()->with('status', 'Reliability test started');
    }
}
