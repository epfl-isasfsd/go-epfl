<?php

namespace App\Http\Controllers;

use DateTime;
use App\Rules;
use App\Models\Url;
use App\Models\User;
use App\Models\Alias;
use App\Mail\NewAlias;
use App\Mail\EditAlias;
use App\Mail\DeleteAlias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Helpers\Telegram;

class AliasController extends Controller
{
    public function create(Request $request)
    {
        $aliasInput      = $request->input('alias-new');
        $urlInput        = $request->input('url-new');
        $hiddenAlias     = $request->has('hidden-new');
        $timeToLive      = $request->input('ttl-new');

        $user_id = Auth::id();

        $url   = Url::firstOrCreate(['url' => $urlInput]);
        $alias = new Alias;
        $alias->alias = $aliasInput;
        $alias->url()->associate($url);
        $alias->hidden = $hiddenAlias;
        if ($timeToLive > 0)
            $alias->obsolescence_date = date('Y-m-d', strtotime('+'.$timeToLive.' months'));

        $validator = $alias->validate();
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $alias->save();
        $alias->owners()->attach($user_id);

        Mail::to(config('mail.admins'))
            ->send(new NewAlias($alias, Auth::user()));

        $tg = new Telegram;
        $tg->aliasCreated($alias, Auth::user());

        return view('aliasconfirm', ['new_url' => config('app.url').$aliasInput, 'alias' => $alias, 'page' => 'home']);
    }

    public function aliasObject($alias) : Alias
    {
        if ($alias = Alias::where(['alias' => $alias])->first())
            return $alias;
        else
            return new Alias();
    }

    public function aliasEmpty(Alias $alias) : bool
    {
        if ($alias->is(new Alias()))
            return true;
        else
            return false;

    }

    public function userOwns(Alias $alias)
    {
        return $alias->owners->contains(Auth::user());
    }

    public function edit($alias)
    {
        if ($this->aliasEmpty( $alias = $this->aliasObject($alias) ))
            abort(404);

        $obsolescence_date = new DateTime($alias->obsolescence_date);

        return $this->userOwns($alias) ? view('alias.alias-edit', [
            'alias'             => $alias->alias,
            'url'               => $alias->url->url,
            'hidden'            => $alias->hidden,
            'obsolescence_date' => $obsolescence_date->format('Y-m-d'),
            'ttl'               => $alias->obsolescence_date ?: null,
            'aliasObj'          => $alias,
            'owners'            => $alias->owners ?: null,
        ]) : abort(403, 'You are not allowed to edit this alias');

    }

    public function update(Request $request)
    {
        $old_alias = $request->input('original_alias');
        $alias = $this->aliasObject($old_alias);
        if ($this->aliasEmpty($alias))
            abort(404);

        if (!$this->userOwns($alias))
            abort(403, 'You are not allowed to edit this alias');

        $hiddenAlias = $request->has('hidden-update');
        $alias->hidden = $hiddenAlias;
        $alias->alias = $request->input('alias-update');
        $timeToLive = $request->input('ttl-update');
        if ($timeToLive > 0) {
            $alias->obsolescence_date = date('Y-m-d', strtotime('+'.$timeToLive.' months'));
        } else {
            $alias->obsolescence_date = null;
        }

        $url = Url::firstOrCreate(['url' => $request->input('url-update')]);
        $alias->url()->associate($url);

        $validator = $alias->validate();
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $alias->save();

        Mail::to(config('mail.admins'))
            ->send(new EditAlias($alias, Auth::user()));

        $tg = new Telegram;
        $tg->aliasUpdated($alias, $old_alias, Auth::user());

        return redirect('profile')->with('status', 'The alias has been updated');
    }

    public function updateOwner(Request $request){
        $owner_email = $request->input('new_owner');
        $alias = Alias::where('alias', $request->input('alias'))->first();
        $ownersArray = [];
        foreach ($alias->owners as $key => $value) {
            array_push($ownersArray, $value->email);
        }
        if (is_null($user = User::where('email', $owner_email)->first())) {
            return redirect()->back()->withErrors("The user with email \"$owner_email\" does not exist");
        }
        if (in_array($owner_email, $ownersArray) && $owner_email == Auth::user()->email) {
            return redirect()->back()->withErrors("You are already an owner of this alias");
        }
        if (in_array($owner_email, $ownersArray)) {
            return redirect()->back()->withErrors($user->firstname . " " . $user->lastname . " is already an owner of this alias");
        }
        $alias->owners()->attach($user);
        $alias->save();
        return redirect()->back()->with('status', $user->firstname . " " . $user->lastname . " has been successfully added to the $alias->alias's owners");
    }

    public function confirmDelete($alias)
    {
        $alias = Alias
                ::where(['alias' => $alias])
                ->withCount('clicks')
                ->first();

        if ($alias->is(new Alias()))
            abort(404);

        $obsolescence_date = new DateTime($alias->obsolescence_date);

        return $this->userOwns($alias) ? view('alias.confirm-alias-delete', [
            'aliasObj'          => $alias,
            'alias'             => $alias->alias,
            'url'               => $alias->url->url,
            'hidden'            => $alias->hidden,
            'obsolescence_date' => $obsolescence_date->format('Y-m-d'),
            'ttl'               => $alias->obsolescence_date ?: null,
            'numClicks'         => $alias->clicks_count,
            'owners'            => $alias->owners,
        ]) : abort(403, 'You are not allowed to delete this alias');
    }

    public function delete($alias)
    {
        $alias = Alias
                ::where(['alias' => $alias])
                ->withCount('clicks')
                ->first();

        $authUser = Auth::user();

        $success_message = "Alias \"$alias->alias\" successfully deleted.";

        Mail::to(config('mail.admins'))
            ->send(new DeleteAlias($alias, Auth::user(), "admins"));

        $ownersArray = [];
        foreach ($alias->owners as $key => $value) {
            if ($value->email != Auth::user()->email) {
                array_push($ownersArray, $value->email);
            }
        }

        Mail::to(Auth::user()->email)
            ->cc($ownersArray)
            ->send(new DeleteAlias($alias, Auth::user(), "owners"));

        $tg = new Telegram;
        $tg->aliasDeleted($alias, Auth::user());

        // need to remove the fk permissions first...
        \DB::table('permissions')
            ->where('alias_id', $alias->id)
            ->delete();

        \DB::table('flags')
            ->where('object_id', $alias->id)
            ->where('object_type', 'App\\Models\\Alias')
            ->delete();

        $alias->delete();

        return redirect('/profile')->with('status', "Alias \"$alias->alias\" successfully deleted.");
    }
}
