<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class Alias extends Model
{
    use Notifiable;

    /**
     * Get all of the alias' flags.
     */
    public function flags()
    {
        return $this->morphMany('App\Models\Flag', 'object');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function scopePublic($query)
    {
        return $query->where('hidden', 0)->has('flags', '<', config('app.autoreportthreshold'));
    }

    public function scopeSearch($query, $search)
    {
        if (! $search) {
            return $query;
        }

        return $query->where('url', 'like', "%$search%")
            ->orWhere('alias', 'like', "%$search%");
    }

    public function owners()
    {
        return $this->belongsToMany('App\Models\User', 'permissions');
    }

    public function url()
    {
        return $this->belongsTo('App\Models\Url');
    }

    public function clicks()
    {
        return $this->hasMany('App\Models\Log');
    }

    public function referrers()
    {
        return $this->hasMany('App\Models\Log');
    }

    public function validate()
    {
        $urlValidator = $this->url->validate();

        if ($urlValidator->fails()) {
            return $urlValidator;
        }

        // Note: the SQL query 
        // `select char_length(alias) as l, alias, id from aliases order by l desc limit 10;`
        // teach us that the longest alias is currently 81 chars.
        // In order to limit XSS possibilities, alias length are now allowed
        // to be max 2x42 chars, 84.
        return Validator::make(
            array(
                'alias' => $this->alias
            ),
            array(
                'alias' => ['required', Rule::unique('aliases')->ignore($this->id),
                            Rule::unique('aliases', 'code'),
                            'max:84', new \App\Rules\Alias(), new \App\Rules\Blacklist()]
            )
        );
    }
}
