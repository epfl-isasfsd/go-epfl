<?php

namespace App\Helpers;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

trait QRHelper
{
    private function QRcode($color = "Black", $backgroundcolor = "White", $format="svg", $size, $style = "square", $margin)
    {
        $red = 0;
        $green = 0;
        $blue = 0;

        $backgroundRed = 255;
        $backgroundGreen = 255;
        $backgroundBlue = 255;

        switch($color) {
            case "red":
                $red = 255;
            break;
            case "green":
                $green = 255;
            break;
            case "blue":
                $blue = 255;
            break;
        }

        switch($backgroundcolor) {
            case "black":
                $backgroundRed = 0;
                $backgroundGreen = 0;
                $backgroundBlue = 0;
            break;
            case "red":
                $backgroundRed = 255;
                $backgroundGreen = 0;
                $backgroundBlue = 0;
            break;
            case "green":
                $backgroundRed = 0;
                $backgroundGreen = 255;
                $backgroundBlue = 0;
            break;
            case "blue":
                $backgroundRed = 0;
                $backgroundGreen = 0;
                $backgroundBlue = 255;
            break;
        }

        return QrCode::format($format)
            ->size($size)
            ->margin($margin)
            ->color($red, $green, $blue)
            ->backgroundColor($backgroundRed, $backgroundGreen, $backgroundBlue)
            ->style($style)
            ->encoding('UTF-8')
            ->generate(config('app.url').$this->alias->alias);
    }
}