<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ApiHelper
{
    private static function _checkToken($token, $is_bearer=true)
    {
        if (empty($token)) {
          $token = 'wrong';
        }
        try {
            if ($is_bearer) {
                $label_token = 'token';
                $token = "Bearer " . $token;
            } else {
                $label_token = 'api_token';
            }
            $user = User::where($label_token, $token)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $message = ($token === 'wrong')
          ? __('You need to provide a "token" entry.')
          : __('The "token" you provided is not valid.');
          
            return response()
              ->json([
                  'status' => 'forbidden',
                  'message' => $message
                ], Response::HTTP_FORBIDDEN)
              ->header('Access-Control-Allow-Origin', '*')->send();
        }
        return $user;
    }

    /**
     * Check the user token, i.e. the token that the user define in his profile
     */
    public static function checkUserToken(Request $request)
    {
        $token = $request->input('token', 'wrong');
        return ApiHelper::_checkToken($token, /*is_bearer=*/false);
    }

    /**
     * Check Bearer token, i.e. the token used by React Api call
     */
    public static function checkBearerToken(Request $request)
    {
        $token = $request->bearerToken();
        if (empty($token) || $token == 'null') {
            $token = 'wrong';
        }
        return ApiHelper::_checkToken($token);
    }
}
