Feature:
    It is possible to see the list of aliases on the website

    Scenario: The browse page is accessible
        When I am on "/browse"
        Then the response status code should be 200
        And I should see "Browse links"

    Scenario: The browse page contains aliases table
        When I am on "/browse"
        Then I should see an "table#aliases" element

    @database
    Scenario: We can access the info page for each alias
        Given "ggl" alias exists for "https://google.com"
        When I am on "http://go.epfl.ch/info/ggl"
        Then the response status code should be 200

    @database
    Scenario: We can see the QR Code container
        Given "ggl" alias exists for "https://google.com"
        When I am on "http://go.epfl.ch/info/ggl"
        Then should see "QR Code" in the "div.box#qrcode-container" element

    @database
    Scenario: We can see the QR Code
        Given "ggl" alias exists for "https://google.com"
        When I am on "http://go.epfl.ch/info/ggl"
        Then should see an "img#qrcode" element
