Feature:
    API tokens secures POST requests via API

    Scenario: A user can generate a token
        Given I am a sample user
        When I am on "profile"
        And I follow "Generate token"
        Then I should be on "profile"
        And I should see "Token: "

    Scenario: A user can regenerate a token
        Given I am a sample user
        And my API token is "Fae5boo3zehiosu5"
        When I am on "profile"
        And I follow "Regenerate"
        Then I should be on "profile"
        And I should not see "Token: Fae5boo3zehiosu5"

    Scenario: A user can delete a token
        Given I am a sample user
        And my API token is "Fae5boo3zehiosu5"
        When I am on "profile"
        And I follow "Delete"
        Then I should be on "profile"
        And I should not see "Token: "
